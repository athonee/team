package com.cpt202.team.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List; //(在private之后)新加的
import java.util.ArrayList; //新加的
import com.cpt202.team.models.Team; //新加的

//为了告诉framework下面的class是controller, then Spring knows that this controller可以接受HTTP request
@RestController
@RequestMapping("/team")
public class TeamController {
    private List<Team> teams = new ArrayList<Team>();  //initialize初始化?看不懂,video1-29:25

    @GetMapping("/team/list")
    //url?(可能指/team/list)
    //get map告诉Spring如果收到一个请求,并且请求是/team/list,那么“public String"方法应该执行
    public List<Team> getList(){
        return teams;//返回Team的list
    }
    //make a Get requset using browser
    //以上，是Add a RestController to the project to respond to a GET request（共四种requests）

    @PostMapping("/team/add") //映射
    public void addTeam(@RequestBody Team team){   
        //RequsetBody可以将string转换为object
        //Team是一个object而不是string(包括name和membercount),详见team.java
        teams.add(team);
    }
    //make a Post request using Postman(一个软件)

    //JSON present an object. (string在JSON中如何work
    //format: {"name": "Jason",
    // "membercount": 6} 
    //可以用JSON格式形成string，把它和post映射一起pass request，RequestBody会将Jason转化为team对象
}
